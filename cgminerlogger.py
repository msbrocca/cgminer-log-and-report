__author__ = 'maxivm'

import threading
import logging
import time
import os
import settings
import cgminerreport


def log():
    logging.debug("Suppose that I'm Logging to a file")
    # Create folder is it doesn't exist
    if settings.LOGGING_FOLDER_LOCATION is not None and settings.LOGGING_FOLDER_LOCATION != '':
        if not os.path.exists(settings.LOGGING_FOLDER_LOCATION):
            os.makedirs(settings.LOGGING_FOLDER_LOCATION)
    f = open(settings.LOGGING_FOLDER_LOCATION + 'cgminerlog.log', 'a')
    f.write('---------------------------------------------------------\n')
    f.write(time.ctime() + cgminerreport.get_logging_report())
    f.flush()
    f.close()
    threading.Timer(settings.LOGGING_INTERVAL, log).start()