__author__ = 'maxivm'

from cgminer_wrapper import CgminerAPI
import settings
import logging

STATUS_MAP = {
    'W': 'Warning',
    'I': 'Informational',
    'S': 'Success',
    'E': 'Error',
    'F': 'Fatal (code bug)'
}

SHARED_REPORT = """
STATUS: {0} | MESSAGE: {1} | DESCRIPTION: {2} {3}
"""


def _get_complete_report():
    try:
        connector = CgminerAPI()
        #Api result example
        #result = {u'STATUS': [{u'STATUS': u'S', u'Msg': u'1 GPU(s) - 0 ASC(s) - 0 PGA(s) - ', u'Code': 9, u'When': 1390875460, u'Description': u'cgminer 3.4.0'}], u'DEVS': [{u'Difficulty Accepted': 889018.0, u'Temperature': 62.0, u'Difficulty Rejected': 7192.0, u'GPU Voltage': 1.225, u'GPU Clock': 1050, u'Fan Speed': 3651, u'Status': u'Alive', u'Device Rejected%': 0.7988, u'Fan Percent': 90, u'Rejected': 232, u'Memory Clock': 1500, u'Hardware Errors': 0, u'Accepted': 28678, u'Last Share Pool': 0, u'Diff1 Work': 900325, u'Total MH': 59317.8952, u'Enabled': u'Y', u'Device Hardware%': 0.0, u'Last Valid Work': 1390875452, u'Last Share Time': 1390875452, u'GPU': 0, u'MHS av': 0.64, u'Last Share Difficulty': 31.0, u'MHS 5s': 0.71, u'GPU Activity': 99, u'Intensity': u'13', u'Powertune': 0, u'Utility': 18.6}], u'id': 1}
        return connector.command('devs')
    except Exception as ex:
        logging.error("Error connecting to CGMINER. Error: " + str(ex))
        print "Check if CGMINER is running. We couldn't connect to it"
        exit(1)


def get_logging_report():
    return _get_report(settings.LOGGING_DESIRED_VALUES, '\n')


def get_email_report():
    return _get_report(settings.REPORT_DESIRED_VALUES, '<br>')


def _get_report(values_list, separator):
    all_info = _get_complete_report()
    report = _get_shared_report(all_info, separator)
    report += '\n'
    gpus_info = all_info.get('DEVS')
    for gpu in gpus_info:
        for value in values_list:
            report = report + value + ': ' + str(gpu.get(value)) + separator
    return report


def _get_shared_report(report, separator):
    info = report.get('STATUS', None)[0]
    if info:
        status = info.get('STATUS')
        message = info.get('Msg')
        desc = info.get('Description')
        return SHARED_REPORT.format(STATUS_MAP.get(status), message, desc, separator)