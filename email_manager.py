
import logging
import smtplib
import base64
from email.mime.text import MIMEText
import settings

PLAIN_TEXT_EMAIL = 'plain'
HTML_TEXT_EMAIL = 'html'


def send_mail(email_body, email_subject, to_email, email_body_content_type='HTML'):
    """
        Method responsible for sending emails
        Receive:
          email_body_content_type = HTML or PLAIN (you can use email_sender.HTML_TEXT_EMAIL and email_sender.PLAIN_TEXT_EMAIL)
          email_subject = subject of the email
          email_to_list = list of destinations emails (Ie: ['maxi.sbrocca@gmail.com','gaston.robledo@santexgroup.com'])
          to_email = string de direcciones separada por comas Ej: 'maxi.sbrocca@gmail.com,gaston.robledo@santexgroup.com'
    """
    logging.info("inside send_email method")
    msg = MIMEText(email_body, email_body_content_type)
    logging.info("creating the message")
    msg['Subject'] = email_subject
    if to_email:
        msg['To'] = to_email + ',' + settings.REPORT_EMAIL
    else:
        msg['To'] = settings.REPORT_EMAIL
    logging.info("account authentication")
    mail_server = smtplib.SMTP(settings.REPORT_EMAIL_SMTP_ADDRESS, settings.REPORT_EMAIL_SMTP_PORT)
    mail_server.ehlo()
    mail_server.starttls()
    mail_server.ehlo()
    mail_server.login(settings.REPORT_EMAIL, base64.decodestring(settings.REPORT_EMAIL_ENCRYPTED_PASSWORD))
    logging.info("sending email")
    mail_server.sendmail(settings.REPORT_EMAIL, msg['To'].split(','), msg.as_string())
    logging.info("closing account")
    mail_server.close()
