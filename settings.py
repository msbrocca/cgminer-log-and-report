__author__ = 'maxivm'

# ************************************** LOGGING SETUP PARAMETERS

LOGGING_AVAILABLE = True
#In seconds
LOGGING_INTERVAL = 2

# This is optional - Folder should exist
LOGGING_FOLDER_LOCATION = 'c:\\cgminerlogger\\'

# List all the GPU values you want to log:
#
# 'Difficulty Accepted', 'Temperature', 'Difficulty Rejected',
# 'GPU Voltage', 'GPU Clock', 'Fan Speed', 'Status',
# 'Device Rejected%', 0'Fan Percent', 'Rejected', Memory Clock',
# 'Hardware Errors', 'Accepted', 'Last Share Pool', 'Diff1 Work',
# 'Total MH', 'Enabled', 'Device Hardware%', 'Last Valid Work',
# 'Last Share Time', 'GPU', 'MHS av', 'Last Share Difficulty',
# 'MHS 5s', 'GPU Activity', u'Intensity', 'Powertune', 'Utility'
LOGGING_DESIRED_VALUES = ['GPU Voltage', 'GPU Activity', 'Temperature', 'Fan Speed']


# ************************************** LOGGING SETUP PARAMETERS

REPORT_AVAILABLE = True
#Seconds
REPORT_INTERVAL = 10
REPORT_EMAIL = 'your@email.here'
# Use encrypt_emailpassword.py to encrypt your email password
REPORT_EMAIL_ENCRYPTED_PASSWORD = 'your-encripted-psw-here'
REPORT_EMAIL_SMTP_ADDRESS = 'smtp.gmail.com'
REPORT_EMAIL_SMTP_PORT = 587
REPORT_EMAIL_SUBJECT = ''
# Use REPORT_EMAIL_DISTLIST to create a comma-separated list of email addresses you want to distribute the report
# Ie: 'email1@gmail.com,email2@gmail.com,email3@gmail.com'
# Left is blank to receive the report just to REPORT_EMAIL address
REPORT_EMAIL_DISTLIST = ''

# List all the GPU values you want to log:
#
# 'Difficulty Accepted', 'Temperature', 'Difficulty Rejected',
# 'GPU Voltage', 'GPU Clock', 'Fan Speed', 'Status',
# 'Device Rejected%', 0'Fan Percent', 'Rejected', Memory Clock',
# 'Hardware Errors', 'Accepted', 'Last Share Pool', 'Diff1 Work',
# 'Total MH', 'Enabled', 'Device Hardware%', 'Last Valid Work',
# 'Last Share Time', 'GPU', 'MHS av', 'Last Share Difficulty',
# 'MHS 5s', 'GPU Activity', u'Intensity', 'Powertune', 'Utility'
REPORT_DESIRED_VALUES = ['GPU Voltage', 'GPU Activity', 'Temperature', 'Fan Speed']
