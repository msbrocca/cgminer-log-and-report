# CGMINER LOG AND REPORT

This is a python tool to run in the same rig where CGMINER is running. You can set up cgminer-log-and-report to log to a file every X seconds the information you want and to send an email every Y seconds also with the information you want. In this way, you can receive information instead of connect remotely to the rig (that causes a performance decrease).

## So...

## Why LOG?

Because you can log to a OS file useful information that will allow you create a mining history. Then, you can use that data to generate information to make correct decisions

## Why REPORT?

Because you also can set up the tool to receive information in your email inbox instead connect to the rig to see how is going all.

## Steps to run cgminer-log-and-report

	1.- Install python 2.7.x in your rig (select the appropriate version from here http://www.python.org/download/)

	2.- Set up cgminer-log-and-report to be used. To do this, edit settings.py file:

		2.1.- Logging Setup
		
			2.1.1.- LOGGING_AVAILABLE: True if you want to log. False if not

			2.1.2.- LOGGING_INTERVAL: time interval to log info to the file (in seconds)

			2.1.3.- LOGGING_FOLDER_LOCATION: folder where you want to save the .log file. Left '' so save it in cgminer-log-and-report main folder

			2.1.4.- LOGGING_DESIRED_VALUES: list of the GPU values you want to log. Is a comma separated list. I.e: ['GPU Voltage', 'GPU Activity', 'Temperature', 'Fan Speed'] (follow this pattern)

		2.2.- Report Setup

			2.2.1.- REPORT_AVAILABLE: True if you want receive email information. False if not

			2.2.2.- REPORT_INTERVAL: time interval to send info to email (in seconds)

			2.2.3.- REPORT_EMAIL: the email account to be used

			2.2.4.- REPORT_EMAIL_ENCRYPTED_PASSWORD: the account password encrypted.

				2.2.4.1.- To encrypt your email account password, edit encrypt_emailpassword.py file changing the text 'your-password-here' for your email password. I.e: From: print base64.encodestring('your-password-here') to: print base64.encodestring('password1234')

				2.2.4.2.- Run the script. I.e (Windows cmd): c:\cgminer-log-and-report>C:\Python27\python.exe encrypt_emailpassword.py

				2.2.4.3.- Copy the output and use it in REPORT_EMAIL_ENCRYPTED_PASSWORD (between quotes)

			2.2.5.- REPORT_EMAIL_SMTP_ADDRESS: smtp address. By default, I used gmail smtp because I use an gmail account (recommended)

			2.2.6.- REPORT_EMAIL_SMTP_PORT: smtp port fo your server. Gmail smtp port by default.

			2.2.7.- REPORT_EMAIL_SUBJECT: this allows you to personalize the email subject. This will be useful when you have several rigs (for each rig you will be able to set a customized email subject)

			2.2.8.- REPORT_EMAIL_DISTLIST: this parameter allows you to specify a comma-separated list of email addresses you want to use to distribute the report. I.e: 'email1@gmail.com,email2@gmail.com,email3@gmail.com'. If you left an empty value ('') the report will be sent just to the REPORT_EMAIL address
		
			2.2.9.- REPORT_DESIRED_VALUES: list of the GPU values you want to receive by email. Is a comma separated list. I.e: ['GPU Voltage', 'GPU Activity', 'Temperature', 'Fan Speed'] (follow this pattern)

	3.- Once you finished the tool setup, you can run the main.py file (cgminer should be running first). I.e (Windows cmd): c:\cgminer-log-and-report>C:\Python27\python.exe main.py

	* BIG NOTE HERE: cgminer script should be started adding this two new parameters: --api-listen --api-allow W:0/0
	* If you forgot those params, CgminerAPI wont be able to connect against cgminer

	4.- That's all. cgminer-log-and-report should be working. If not, send me an email to maxi.sbrocca@gmail.com, or create an issue in the Bitbacket project: https://bitbucket.org/msbrocca/cgminer-log-and-report/


I hope this basic script tool helps you with your minig tasks.

If you think it deserves a donation, this is my Litecoin wallet: LTxAA9Qqkqu71ip6JQhoJtV4rL58GLuKXj (BTC wallet comming soon jeje)




