__author__ = 'maxivm'

import settings
import cgminerlogger
import cgminerreporter

if settings.LOGGING_AVAILABLE:
    cgminerlogger.log()

if settings.REPORT_AVAILABLE:
    cgminerreporter.report()