__author__ = 'maxivm'

import threading

import email_manager
import settings
import cgminerreport


def report():
    body = cgminerreport.get_email_report()
    subject = 'CGMINER REPORT' if settings.REPORT_EMAIL_SUBJECT is None or \
        settings.REPORT_EMAIL_SUBJECT == '' else settings.REPORT_EMAIL_SUBJECT
    email_manager.send_mail(
        email_body=body,
        email_subject=subject,
        to_email=settings.REPORT_EMAIL_DISTLIST,
    )
    threading.Timer(settings.REPORT_INTERVAL, report).start()